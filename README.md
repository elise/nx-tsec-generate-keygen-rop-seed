# NX TSEC Generate Keygen ROP Seed

A tool to ~~bruteforce~~generate an input seed to obtain ROP under keygen.

## Usage

### Prerequesites

* [Rust](https://rustup.rs)
* CSecret 0

You need to edit const values at the top of in main.rs (They have doc comments). Then run:

```sh
cargo run --release
```
