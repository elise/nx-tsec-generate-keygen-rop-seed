// This could be a lot more efficient but the AES crate used is pretty bad.

use simple_rijndael::{impls::RijndaelCbc, paddings::ZeroPadding};
use time::OffsetDateTime;

type Cipher = RijndaelCbc<ZeroPadding>;

/// Set this to the address you want to obtain ROP under
const WANTED_ADDRESS: u32 = 0x94d;
/// Set this to the return address that is pushed to the stack
const UNCONTROLLED_ADDRESS: u32 = 0x929;
/// Set this to the signature of the keygen payload
const SIG: &[u8; 0x10] = &[0u8; 0x10];
/// Set this to the value of csecret 0
const CSECRET: &[u8; 0x10] = &[0u8; 0x10];
/// Leave this as is
const IV: &[u8; 0x10] = &[0u8; 0x10];

fn keygen_algo(seed: Vec<u8>, sig: &[u8]) -> Vec<u8> {
    let stage_1 = Cipher::new(CSECRET, 16).unwrap().encrypt(IV, seed).unwrap();

    Cipher::new(&stage_1, 16)
        .unwrap()
        .encrypt(IV, sig.to_vec())
        .unwrap()
}

struct BruteforceRes {
    pub seed: [u8; 0x10],
    pub output: [u8; 0x10],
}

fn bruteforce() -> BruteforceRes {
    let base = UNCONTROLLED_ADDRESS.to_le_bytes();
    let csecret_cipher = Cipher::new(CSECRET, 16).unwrap();
    loop {
        let random_key: [u8; 4] = rand::random();

        let seed: [u8; 0x10] = [
            base[0],
            base[1],
            base[2],
            base[3],
            0x66,
            0x66,
            0x66,
            0x00,
            0x66,
            0x66,
            0x66,
            0x00,
            random_key[0],
            random_key[1],
            random_key[2],
            random_key[3],
        ];

        let encrypted_seed = csecret_cipher.encrypt(IV, seed.to_vec()).unwrap();
        let output = Cipher::new(&encrypted_seed, 16)
            .unwrap()
            .encrypt(IV, SIG.to_vec())
            .unwrap();

        let first_u32 = u32::from_le_bytes(output[0..4].try_into().unwrap());
        let final_u32 = u32::from_le_bytes(output[12..16].try_into().unwrap());

        let final_u32_reg = final_u32 >> 0x10 & 0b111;

        /*
            Output parameters:

                * First u32 is the first return address. Code virtual addresses appear to have a 16 bit width so we only need to ensure that the first 16 bits of our address is valid.
                * Final u32 is the xdld parameter. It needs to be: 0b001XXXXXXXXXXXX0000
        */
        if (first_u32 % 0x10000 == WANTED_ADDRESS)
            && (final_u32_reg == 1)
            && (final_u32 & 0b1111 == 0)
        {
            println!("Exfiltration address: {:#X}", final_u32 % 0x4000);
            return BruteforceRes {
                output: output.try_into().unwrap(),
                seed,
            };
        }
    }
}

fn main() {
    let start = OffsetDateTime::now_utc();

    let res = bruteforce();
    println!(
        "Bruteforced a valid key in {} milliseconds",
        (OffsetDateTime::now_utc() - start).whole_milliseconds()
    );

    println!(
        "Seed: {}\nOutput: {}",
        hex::encode(&res.seed),
        hex::encode(&res.output)
    );

    let r13 = u32::from_le_bytes(res.seed[12..=15].try_into().unwrap());
    let r14 = u32::from_le_bytes(res.seed[8..=11].try_into().unwrap());
    let r15 = u32::from_le_bytes(res.seed[4..=7].try_into().unwrap());
    let expected_return = u32::from_le_bytes(res.seed[0..=3].try_into().unwrap());

    println!("Exploitation:\n\tr13: {:#X}\n\tr14: {:#X}\n\tr15: {:#X}\n\texpected_keygen_init_return_addr: {:#X}", r13, r14, r15, expected_return);
    assert_eq!(
        keygen_algo(res.seed.to_vec(), SIG).as_slice(),
        res.output.as_slice()
    );
}
